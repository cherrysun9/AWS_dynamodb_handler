from __future__ import print_function
import boto3
import json
import sys
from boto3.dynamodb.conditions import Key,Attr
from decimal import Decimal

# Note - Do not change the class name and constructor
# You are free to add any functions to this class without changing the specifications mentioned below.
class DynamoDBHandler:

    def __init__(self, region):
        self.client = boto3.client('dynamodb')
        self.resource = boto3.resource('dynamodb', region_name=region)

    def create_movie_table(self):
        dynamodb = boto3.resource('dynamodb')
        table = dynamodb.create_table(
            TableName='Movies',
            KeySchema=[
                {
                    'AttributeName': 'year',
                    'KeyType': 'HASH'  # Partition key
                },
                {
                    'AttributeName': 'title',
                    'KeyType': 'RANGE'  # Sort key
                }
            ],
            AttributeDefinitions=[
                {
                    'AttributeName': 'year',
                    'AttributeType': 'N'
                },
                {
                    'AttributeName': 'title',
                    'AttributeType': 'S'
                },

            ],
            ProvisionedThroughput={
                'ReadCapacityUnits': 10,
                'WriteCapacityUnits': 10
            }
        )
        waiter = self.client.get_waiter('table_exists')
        waiter.wait(TableName='Movies')
        return table

    def load_movies(self, movies_list):
        dynamodb = boto3.resource('dynamodb')
        table = dynamodb.Table('Movies')
        for movie in movies_list:
            movie ["info"]["lower_title"] = movie['title'].lower()
            if "actors" in movie['info']:
                actor_lower = [x.lower() for x in movie['info']['actors']]
                movie ['info']['lower_actors'] = actor_lower 
            if "directors" in movie['info']:
                director_lower = [x.lower() for x in movie['info']['directors']]
                movie ['info']['lower_directors'] = director_lower
            table.put_item(Item=movie)


    def insert_movie(self, title, year, directors, actors, release_date, rating):
        dynamodb = boto3.resource('dynamodb')
        table = dynamodb.Table('Movies')
        if not title or not year or not directors or not actors or not release_date or not rating:
            return ("Movie %s could not be inserted - <missing parameters>" % title)

        lower_title = title.lower()
        actor_lower =[]
        director_lower = []

        for e in actors:
            actor_lower.append(e.lower())
        
        for e in directors:
            director_lower.append(e.lower())
        try:
            response = table.put_item(
                Item={
                    'year' : year,
                    'title' : title,
                    'info':{
                        'directors' : directors,
                        'actors' : actors,
                        'release_date': release_date,
                        'rating': Decimal(rating),
                        'lower_title': lower_title,
                        'lower_directors': director_lower,
                        'lower_actors': actor_lower 
                    }
                }
            )
        except:
            return ("Movie %s could not be inserted - <dynamodb error>" %  title)
        return ("Movie %s successfully inserted" % title)


    def delete_movie(self, title):
        temp = title
        title = title.lower()
        dynamodb = boto3.resource('dynamodb')
        table = dynamodb.Table('Movies')
        response = table.scan(
                FilterExpression = Attr('info.lower_title').eq(title)
            )
        result = response['Items']
        while 'LastEvaluatedKey' in response:
            response = table.scan(
                ExclusiveStartKey = response['LastEvaluatedKey'],
                FilterExpression = Attr('info.lower_title').eq(title)
            )
        result.extend(response['Items'])

        if(len(result) == 0):
            return ("Movie %s does not exist" %  temp)
        with table.batch_writer() as batch:
            for item in result:
                batch.delete_item(
                    Key={'title':item['title'],
                         'year' :item['year']
                    }
                )
        return ("Movie %s successfully deleted" % temp)


    def search_movie(self, actors):
        dynamodb = boto3.resource('dynamodb')
        table = dynamodb.Table('Movies')
        actors = actors.lower()
        a = actors
        response = table.scan(
                FilterExpression = Attr('info.lower_actors').contains(actors)
            )
        result = response['Items']
        while 'LastEvaluatedKey' in response:
            response = table.scan(
                ExclusiveStartKey = response['LastEvaluatedKey'],
                FilterExpression = Attr('info.lower_actors').contains(actors)
            )

        result.extend(response['Items'])
        if(len(result) == 0):
            return ("No movies found for actors %s" % a)
        output = []
        for each in result:
            output.append({"info": {"actors": each['info']['actors']}, "title": each['title'],"year": str(each['year'])})
        return output


    def search_move_actor_director(self, actors, directors):
        dynamodb = boto3.resource('dynamodb')
        table = dynamodb.Table('Movies')
        a = actors
        b = directors
        actors = actors.lower()
        directors = directors.lower()
        response = table.scan(
                FilterExpression = Attr('info.lower_actors').contains(actors) & Attr('info.lower_directors').contains(directors)
            )
        result = response['Items']
        while 'LastEvaluatedKey' in response:
            response = table.scan(
                ExclusiveStartKey = response['LastEvaluatedKey'],
                FilterExpression = Attr('info.lower_actors').contains(actors) & Attr('info.lower_directors').contains(directors)
            )
        result.extend(response['Items'])
        if(len(result) == 0):
            return ("No movies found for actor %s and director %s" %(a ,b))
        output = []
        for each in result:
            output.append({"info": {"actors": each['info']['actors'], "directors": each['info']['directors']}, "title": each['title'],"year": str(each['year'])})
        return output


    def print_movie(self, option):
        dynamodb = boto3.resource('dynamodb')
        table = dynamodb.Table('Movies')
        
        response = table.scan()
        result = response['Items']
        while 'LastEvaluatedKey' in response:
            response = table.scan(
                ExclusiveStartKey = response['LastEvaluatedKey'])
        result.extend(response['Items'])

        maxr = Decimal("0")
        minr = Decimal("100")
        output = []
        for each in result:
            if 'rating' in each['info']:
                if each['info']['rating'] > maxr:
                    maxr = each['info']['rating']
                if each['info']['rating'] < minr:
                    minr = each['info']['rating']

        if option == "highest_rating_movie":
            for each in result:
                if 'rating' in each['info']:
                    if each['info']['rating'] - maxr == 0:
                        output.append({"title": each['title'],"year": str(each['year']),"info": {"actors": each['info']['actors'], "directors": each['info']['directors'], "rating": str(maxr)}})
        
        elif option == "lowest_rating_movie":
            for each in result:
                if 'rating' in each['info']:
                    if each['info']['rating'] - minr == 0:
                        output.append({"title": each['title'],"year": str(each['year']),"info": {"actors": each['info']['actors'], "directors": each['info']['directors'], "rating": str(minr)}})
        return output

    def update(self, title, year, directors, actors, release_date, rating):
        dynamodb = boto3.resource('dynamodb')
        table = dynamodb.Table('Movies')
        temp = title.lower()
        actor_lower =[]
        director_lower = []
        for e in actors:
            actor_lower.append(e.lower())
        
        for e in directors:
            director_lower.append(e.lower())

        check = table.query(
            KeyConditionExpression=Key('title').eq(title) & Key('year').eq(year)
        )
        if len(check['Items']) == 0:
            return ("Movie %s and %s does not exist" % (title, year))
        try:
            response = table.update_item(
                Key={
                    'year': year,
                    'title': title
                },
                UpdateExpression="set info.rating=:r, info.actors=:a, info.directors=:d, info.lower_title=:lt, info.lower_directors=:ld, info.release_date=:rd, info.lower_actors=:la",
                ExpressionAttributeValues={
                    ':r': Decimal(rating),
                    ':a': actors,
                    ':d': directors,
                    ':lt': temp,
                    ':ld': director_lower,
                    ':rd': release_date,
                    ':la': actor_lower
                },
                ReturnValues="UPDATED_NEW"
            )
        except:
            return ("Movie %s could not be updated - <dynamodb error>" %  title)

        return ("Movie %s successfully updated" % title)

    def delete_movie_table(self, table_name):
        dynamodb = boto3.resource('dynamodb')
        try:
            table = dynamodb.Table(table_name)
            table.delete()
        except:
            return ("Table %s does not exist" % table_name)
        
        return ("Table %s successfully deleted" % table_name)

    def dispatch(self, command_string):
        # TODO - This function takes in as input a string command (e.g. 'insert_movie')
        # the return value of the function should depend on the command
        # For commands 'insert_movie', 'delete_movie', 'update_movie', delete_table' :
        #       return the message as a string that is expected as the output of the command
        # For commands 'search_movie_actor', 'search_movie_actor_director', print_stats' :
        #       return the a list of json objects where each json object has only the required
        #       keys and attributes of the expected result items.

        # Note: You should not print anything to the command line in this function.
        if command_string == "insert_movie": 
            year = input("Year>")
            year = int(year)
            title = input("Title>")
            title = str(title)
            directors = input("Directors>")
            directors = str(directors)
            actors = input("Actors>")
            actors  = str(actors)
            date = input("Release date>")
            date = str(date)
            rating = input("Rating>")
            rating = Decimal(rating)

            parts_d = directors.split(",")
            parts_a = actors.split(",")
            
            #putting the data into list
            d = []
            for e in parts_d:
                d.append(e)
            a = []
            for e in parts_a:
                a.append(e)
            response = self.insert_movie(title, year, d, a, date, rating)
            return response
        elif command_string == "delete_movie":
            title = input("Title>")
            if not title:
                return "Movie could not be deleted - <no title>"
            response = self.delete_movie(title)
            return response
        elif command_string == "search_movie_actor":
            a = input("Actors>")
            if not a:
                return "input field can not be empty"
            response = self.search_movie(a)
            return response
        elif command_string == "search_movie_actor_director":
            a = input("Actors>")
            d = input("Director>")
            response = self.search_move_actor_director(a,d)
            return response
        elif command_string == "update_movie":
            year = input("Year>")
            year = int(year)
            title = input("Title>")
            title = str(title)
            directors = input("Directors>")
            directors = str(directors)
            actors = input("Actors>")
            actors  = str(actors)
            date = input("Release date>")
            date = str(date)
            rating = input("Rating>")
            rating = Decimal(rating)
            parts_d = directors.split(",")
            parts_a = actors.split(",")
            d = []
            for e in parts_d:
                d.append(e)
            a = []
            for e in parts_a:
                a.append(e)
            response = self.update(title, year, d, a, date, rating)
            return response
        elif command_string == "delete_table":
            print("delete_table>")
            name = input("table_name>")
            response = self.delete_movie_table(name)
            return response
        elif command_string == "print_stats":
            print("print_stats>")
            option = input("stat>")
            if option == "highest_rating_movie":
                response = self.print_movie(option)
                return response
            elif option == "lowest_rating_movie":
                response = self.print_movie(option)
                return response
            else:
                return ("Unrecognized statistic query %s" % option)
        else:
            return "Please enter a valid operation"
        return response


def main():
    # TODO - implement the main function so that the required functionality for the program is achieved.
    region = sys.argv[1]
    region = str(region)
    dynamoDB_handler = DynamoDBHandler(region)
    dynamoDB_handler.create_movie_table()
    with open("moviedata.json") as json_file:
        movie_list = json.load(json_file, parse_float=Decimal)

    dynamoDB_handler.load_movies(movie_list)
    #loading the movie to db
    
    while True:
        try:
            command_string = input("Enter command ('help' to see all commands, 'exit' to quit)>")
            if command_string == 'exit':
                print("Good bye!")
                exit()
            elif command_string == 'help':
                print("Supported Commands:")
                print("1. insert_movie")
                print("2. delete_movie")
                print("3. update_movie")
                print("4. delete_table")
                print("4. search_movie_actor")
                print("5. search_movie_actor_director")
                print("6. print_stats")
            else:
                response = dynamoDB_handler.dispatch(command_string)
                if(isinstance(response, str)):
                    print(response)
                else:
                    for each in response:
                        print(json.dumps(each))
        except Exception as e:
            print(e)


if __name__ == '__main__':
    main()
